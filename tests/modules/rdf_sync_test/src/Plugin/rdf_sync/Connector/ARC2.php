<?php

declare(strict_types=1);

namespace Drupal\rdf_sync_test\Plugin\rdf_sync\Connector;

use Drupal\Core\Database\Database;
use Drupal\Core\StringTranslation\TranslatableMarkup;
use Drupal\rdf_sync\Attribute\RdfSyncConnector;
use Drupal\rdf_sync\RdfSyncConnectorPluginPluginBase;
use EasyRdf\GraphStore;
use EasyRdf\Sparql\Result;

/**
 * In memory quad store connector.
 *
 * @phpcs:disable Drupal.NamingConventions.ValidClassName.NoUpperAcronyms
 */
#[RdfSyncConnector(
  id: 'arc2',
  label: new TranslatableMarkup('ARC2'),
  description: new TranslatableMarkup('MySQL-based triplestore with SPARQL support'),
  website: 'https://github.com/semsol/arc2',
)]
class ARC2 extends RdfSyncConnectorPluginPluginBase {

  /**
   * Static cache of ARC2 store.
   */
  protected \ARC2_Store $store;

  /**
   * {@inheritDoc}
   */
  public function query(string $query): Result {
    return $this->doQuery($query);
  }

  /**
   * {@inheritDoc}
   */
  public function update(string $query): Result {
    return $this->doQuery($query);
  }

  /**
   * {@inheritDoc}
   */
  public function clearGraph(string $uri): void {
    $this->doQuery("DELETE FROM <$uri>");
  }

  /**
   * {@inheritDoc}
   */
  public function createGraphStore(): GraphStore {
    throw new \Exception("The '{$this->getPluginId()}' plugin doesn't support ::createGraphStore() implementation");
  }

  /**
   * Runs the query.
   *
   * @param string $query
   *   The query.
   *
   * @return \EasyRdf\Sparql\Result
   *   The EasyRdf result.
   */
  protected function doQuery(string $query): Result {
    $result = $this->getStore()->query($query);
    if (!is_array($result)) {
      throw new \RuntimeException('Query failed: ' . $query);
    }

    $data = match($result['query_type']) {
      'ask' => ['boolean' => $result['result']],
      'delete', 'insert' => ['results' => ['bindings' => []]],
      'select' => $this->processSelectResults($result),
      default => throw new \InvalidArgumentException("The {$this->getPluginId()} connector plugin doesn't support '" . strtoupper($result['query_type']) . "' SPARQL queries"),
    };

    return new Result(json_encode($data), 'application/sparql-results+json');
  }

  /**
   * Processes the results of SELECT query.
   *
   * @param array $result
   *   Results coming from store.
   *
   * @return array[]
   *   Array to be passed to EasyRdf Result object.
   */
  protected function processSelectResults(array $result): array {
    $bindings = [];
    foreach ($result['result']['rows'] ?? [] as $row) {
      $binding = [];
      foreach ($result['result']['variables'] as $variable) {
        $binding[$variable] = [
          'type' => $row["$variable type"],
          'value' => $row[$variable],
        ];
        if (isset($row["$variable lang"])) {
          $binding[$variable]['xml:lang'] = $row["$variable lang"];
        }
      }
      $bindings[] = $binding;
    }
    return [
      'head' => ['link' => [], 'vars' => $result['result']['variables']],
      'results' => [
        'distinct' => FALSE,
        'ordered' => TRUE,
        'bindings' => $bindings,
      ],
    ];
  }

  /**
   * Returns the ARC2 store.
   *
   * @return \ARC2_Store
   *   The ARC2 store.
   */
  protected function getStore(): \ARC2_Store {
    if (!isset($this->store)) {
      $connection = Database::getConnection()->getConnectionOptions();
      $config = [
        'db_host' => $connection['host'],
        'db_name' => $connection['database'],
        'db_user' => $connection['username'],
        'db_pwd' => $connection['password'],
        'db_table_prefix' => $connection['prefix'],
        'db_adapter' => 'pdo',
        'db_pdo_protocol' => 'mysql',
        'store_name' => 'arc',
      ];
      $caller = NULL;
      $this->store = new \ARC2_Store($config, $caller);
      $this->store->createDBCon();
      $this->store->setUp();
    }
    return $this->store;
  }

}
