<?php

declare(strict_types=1);

namespace Drupal\Tests\rdf_sync\Kernel;

use Drupal\Component\Serialization\Yaml;
use Drupal\Core\Entity\ContentEntityInterface;
use Drupal\Core\Entity\EntityStorageException;
use Drupal\KernelTests\KernelTestBase;
use Drupal\Tests\rdf_sync\Traits\RdfSyncTestingDataTrait;
use Drupal\taxonomy\Entity\Term;
use Drupal\taxonomy\Entity\Vocabulary;
use PHPUnit\Framework\Assert;

/**
 * Tests RDF Sync.
 *
 * @group rdf_sync
 */
class RdfSyncTest extends KernelTestBase {

  use RdfSyncTestingDataTrait;

  /**
   * {@inheritdoc}
   */
  protected static $modules = [
    'field',
    'filter',
    'node',
    'rdf_sync',
    'rdf_sync_test',
    'serialization',
    'system',
    'taxonomy',
    'text',
    'user',
  ];

  /**
   * {@inheritdoc}
   */
  protected function setUp(): void {
    parent::setUp();

    $this->installEntitySchema('taxonomy_term');
    $this->installEntitySchema('node');
    $this->installEntitySchema('user');
    $this->installConfig(['rdf_sync_test', 'rdf_sync']);
    $this->installSchema('rdf_sync', ['rdf_sync_uri']);
    $this->installSchema('node', ['node_access']);

    $this->setUpFields();

    // These are API tests where the synchronization is not relevant.
    $this->container->get('rdf_sync.synchronizer')->disableSynchronization();
  }

  /**
   * @covers \Drupal\rdf_sync\RdfSyncUriFieldItemList
   * @covers \Drupal\rdf_sync\RdfSyncMapper
   */
  public function testUriField(): void {
    $term = Term::create([
      'vid' => 'category',
      'name' => $this->randomString(),
      'parent' => [
        Term::create([
          'vid' => 'category',
          'name' => $this->randomString(),
          'uri' => 'http://example.com/category/parent/1',
        ]),
        Term::create([
          'vid' => 'category',
          'name' => $this->randomString(),
          'uri' => 'http://example.com/category/parent/2',
        ]),
      ],
    ]);
    $term->save();

    // Check that URI has been created using the 'default' URI generator.
    $expected = $GLOBALS['base_url'] . '/taxonomy-term/category/' . $term->uuid();
    $term = Term::load($term->id());
    $this->assertSame($expected, $term->get('uri')->value);
    $this->assertSame('http://example.com/category/parent/1', $term->get('parent')->get(0)->entity->get('uri')->value);
    $this->assertSame('http://example.com/category/parent/2', $term->get('parent')->get(1)->entity->get('uri')->value);

    // @todo Re-enable this feature when https://www.drupal.org/i/3425226 lands.
    // @see https://www.drupal.org/i/3425226
    // phpcs:disable
    // Check that the URI cannot be set on a saved entity.
    // $term->set('uri', 'http://example.com');
    // $this->assertSame($expected, $term->get('uri')->value);
    // phpcs:enable

    // Check loading the entity by URI.
    $entity = $this->container->get('rdf_sync.mapper')->getEntityByUri($term->get('uri')->value);
    $this->assertSame($term->id(), $entity->id());
    $this->assertSame($term->label(), $entity->label());
    $this->assertSame($term->get('uri')->value, $entity->get('uri')->value);

    $term = Term::create([
      'vid' => 'category',
      'name' => $this->randomString(),
    ]);

    // Check that setting a custom URI on creation works.
    $term->set('uri', 'http://example.com/foo')->save();
    $term = Term::load($term->id());
    $this->assertSame('http://example.com/foo', $term->get('uri')->value);

    // Check loading the entity by URI.
    $entity = $this->container->get('rdf_sync.mapper')->getEntityByUri('http://example.com/foo');
    $this->assertSame($term->id(), $entity->id());
    $this->assertSame($term->label(), $entity->label());
    $this->assertSame('http://example.com/foo', $entity->get('uri')->value);

    // Change the URI field name to test custom field name but flush all caches
    // to get trigger rebuild of all definitions.
    Vocabulary::load('category')
      ->setThirdPartySetting('rdf_sync', 'uri_field_name', 'custom_uri_field_name')
      ->save();
    drupal_flush_all_caches();

    // Check that a custom name can be used for the URI field.
    $term = Term::load($term->id());
    $this->assertSame('http://example.com/foo', $term->get('custom_uri_field_name')->value);
  }

  /**
   * @covers \Drupal\rdf_sync\Normalizer\RdfSyncNormalizer
   * @covers \Drupal\rdf_sync\Encoder\RdfSyncEncoder
   *
   * @dataProvider providerTestSerialization
   */
  public static function testSerialization(string $format, array $expectedSerialization): void {
    [$term, $node] = static::createEntities();
    $actualTermSerialization = trim(\Drupal::service('serializer')->serialize($term, $format));
    Assert::assertSame($expectedSerialization['taxonomy_term'], $actualTermSerialization);
    $actualNodeSerialization = trim(\Drupal::service('serializer')->serialize($node, $format));
    Assert::assertSame($expectedSerialization['node'], $actualNodeSerialization);
  }

  /**
   * Provides test cases for self::testSerialization())
   *
   * @return array
   *   A list of testing cases.
   *
   * @see self::testSerialization()
   */
  public static function providerTestSerialization(): array {
    $cases = Yaml::decode(file_get_contents(__DIR__ . '/../../fixtures/serialization.yml'));
    return array_map(function (string $format) use ($cases): array {
      return [$format, $cases[$format]];
    }, array_keys($cases));
  }

  /**
   * @covers \Drupal\rdf_sync\RdfSyncMapper::getEntitiesByUris
   */
  public function testGetEntitiesByUris(): void {
    $entityTypes = [
      'taxonomy_term' => ['vid', 'category', 'name', 'Term %s'],
      'node' => ['type', 'page', 'title', 'Node %s'],
    ];

    foreach ($entityTypes as $entityTypeId => [$bundleKey, $bundle, $labelKey, $label]) {
      $storage = $this->container->get('entity_type.manager')->getStorage($entityTypeId);
      for ($i = 1; $i <= 3; $i++) {
        $storage->create([
          $bundleKey => $bundle,
          $labelKey => sprintf($label, $i),
          'uri' => "http://$entityTypeId/$i",
        ])->save();
      }
    }

    $expectedUris = [
      'http://node/3',
      'http://taxonomy_term/2',
      'http://taxonomy_term/3',
      'http://node/1',
      'http://taxonomy_term/1',
      'http://node/2',
    ];
    $actualUris = array_values(array_map(
      fn(ContentEntityInterface $entity): string => $entity->get('uri')->value,
      $this->container->get('rdf_sync.mapper')->getEntitiesByUris($expectedUris)
    ));
    $this->assertSame($expectedUris, $actualUris);
  }

  /**
   * Tests updating the URI.
   *
   * @covers \Drupal\rdf_sync\RdfSyncUriFieldItemList::postSave
   */
  public function testUpdateUri(): void {
    $term = Term::create([
      'vid' => 'category',
      'name' => $this->randomString(),
      'uri' => 'http://example.com/foo',
    ]);
    $term->save();

    $term = Term::load($term->id());
    $this->assertSame('http://example.com/foo', $term->get('uri')->value);

    // We are throwing a logic exception because it is a logical error, but
    // Drupal API handles it as an EntityStorageException as it is part of the
    // postSave() method.
    $this->expectException(EntityStorageException::class);
    // Set to a completely different URI.
    $term->set('uri', 'http://example.com/bar')->save();

    // Change only some case-sensitive characters.
    $term->set('uri', 'http://example.com/Foo')->save();
    $term = Term::load($term->id());
    $this->assertSame('http://example.com/Foo', $term->get('uri')->value);
  }

}
