<?php

declare(strict_types=1);

namespace Drupal\Tests\rdf_sync\Kernel;

use Drupal\KernelTests\KernelTestBase;
use Drupal\Tests\rdf_sync\Traits\RdfSyncTestTrait;
use Drupal\Tests\rdf_sync\Traits\RdfSyncTestingDataTrait;
use Drupal\rdf_sync\Model\SyncMethod;

/**
 * @coversDefaultClass \Drupal\rdf_sync\RdfSyncSynchronizer
 * @group rdf_sync
 */
class RdfSyncSynchronizationTest extends KernelTestBase {

  use RdfSyncTestingDataTrait;
  use RdfSyncTestTrait;

  /**
   * {@inheritdoc}
   */
  protected static $modules = [
    'field',
    'filter',
    'node',
    'rdf_sync',
    'rdf_sync_test',
    'serialization',
    'system',
    'taxonomy',
    'text',
    'user',
  ];

  /**
   * {@inheritdoc}
   */
  protected function setUp(): void {
    parent::setUp();

    $this->installEntitySchema('taxonomy_term');
    $this->installEntitySchema('node');
    $this->installEntitySchema('user');
    $this->installConfig(['rdf_sync_test', 'rdf_sync']);
    $this->installSchema('rdf_sync', ['rdf_sync_uri']);
    $this->installSchema('node', ['node_access']);

    $this->setUpFields();
  }

  /**
   * @covers ::synchronize
   * @dataProvider providerConnectorTestCases
   *
   * @param string $connectorId
   *   The connector plugin ID.
   * @param array $connectorConfig
   *   The connector plugin configuration.
   */
  public function testSynchronization(string $connectorId, array $connectorConfig): void {
    $this->config('rdf_sync.settings')
      ->set('connector.id', $connectorId)
      ->set('connector.config', $connectorConfig)
      ->save();

    $expected = [
      [
        'http://example.com/category/id/1',
        'http://www.w3.org/2004/02/skos/core#inScheme',
        'http://example.com/category',
      ],
      [
        'http://example.com/category/id/1',
        'http://www.w3.org/2004/02/skos/core#prefLabel',
        '"Series"@en',
      ],
      [
        'http://example.com/category/id/1',
        'http://www.w3.org/2004/02/skos/core#definition',
        '"7 seasons"@en',
      ],
      [
        'http://example.com/category/id/1',
        'http://www.w3.org/1999/02/22-rdf-syntax-ns#type',
        'http://www.w3.org/2004/02/skos/core#Concept',
      ],
      [
        'http://example.com/category/id/1',
        'http://www.w3.org/2004/02/skos/core#broaderTransitive',
        'http://example.com/category/parent/1',
      ],
      [
        'http://example.com/category/id/1',
        'http://www.w3.org/2004/02/skos/core#broaderTransitive',
        'http://example.com/category/parent/2',
      ],
      [
        'http://example.com/page/id/1',
        'http://www.w3.org/1999/02/22-rdf-syntax-ns#type',
        'http://example.com/page',
      ],
      [
        'http://example.com/page/id/1',
        'http://example.com/page/title',
        '"Mad Men"@en',
      ],
      [
        'http://example.com/page/id/1',
        'http://example.com/page/category',
        'http://example.com/category/id/1',
      ],
      [
        'http://example.com/page/id/1',
        'http://example.com/page/body',
        '"Strong is stripped while <em>Emphasis</em> is kept"@en',
      ],
    ];

    [$term, $node] = $this->createEntities();
    /** @var \Drupal\rdf_sync\RdfSyncSynchronizer $synchronizer */
    $synchronizer = $this->container->get('rdf_sync.synchronizer');
    $synchronizer->synchronize(SyncMethod::INSERT, [$term, $node], TRUE);
    $this->assertTriples('http://data', $expected);

    $node->delete();
    $term->delete();
    $this->assertNoTriples('http://data', $expected);
  }

}
