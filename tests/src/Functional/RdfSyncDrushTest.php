<?php

declare(strict_types=1);

namespace Drupal\Tests\rdf_sync\Functional;

use Drupal\Tests\BrowserTestBase;
use Drupal\Tests\rdf_sync\Traits\RdfSyncTestTrait;
use Drupal\Tests\rdf_sync\Traits\RdfSyncTestingDataTrait;
use Drush\TestTraits\DrushTestTrait;

/**
 * @coversDefaultClass \Drupal\rdf_sync\Drush\Commands\RdfSyncCommands
 *
 * @group rdf_sync
 */
class RdfSyncDrushTest extends BrowserTestBase {

  use DrushTestTrait;
  use RdfSyncTestingDataTrait;
  use RdfSyncTestTrait;

  /**
   * {@inheritdoc}
   */
  protected $defaultTheme = 'stark';

  /**
   * {@inheritdoc}
   */
  protected static $modules = [
    'rdf_sync',
    'rdf_sync_test',
    'taxonomy',
  ];

  /**
   * {@inheritdoc}
   */
  protected function setUp(): void {
    parent::setUp();
    $this->setUpFields();
  }

  /**
   * @covers ::enableSynchronization
   * @covers ::disableSynchronization
   * @covers ::synchronize
   * @dataProvider providerConnectorTestCases
   *
   * @param string $connectorId
   *   The connector plugin ID.
   * @param array $connectorConfig
   *   The connector plugin configuration.
   */
  public function testCommands(string $connectorId, array $connectorConfig): void {
    $this->config('rdf_sync.settings')
      ->set('connector.id', $connectorId)
      ->set('connector.config', $connectorConfig)
      ->save();

    $this->toggleSynchronizationInDrush('disable');
    $this->toggleSynchronizationInDrush('enable');
    $this->toggleSynchronizationInDrush('disable');

    [$term, $node] = $this->createEntities();

    $this->drush('rdf_sync:synchronize', ['taxonomy_term']);
    $this->assertTriples('http://data', [
      [
        'http://example.com/category/id/1',
        'http://www.w3.org/2004/02/skos/core#inScheme',
        'http://example.com/category',
      ],
      [
        'http://example.com/category/id/1',
        'http://www.w3.org/2004/02/skos/core#prefLabel',
        '"Series"@en',
      ],
      [
        'http://example.com/category/id/1',
        'http://www.w3.org/2004/02/skos/core#definition',
        '"7 seasons"@en',
      ],
      [
        'http://example.com/category/id/1',
        'http://www.w3.org/2004/02/skos/core#broaderTransitive',
        'http://example.com/category/parent/1',
      ],
      [
        'http://example.com/category/id/1',
        'http://www.w3.org/2004/02/skos/core#broaderTransitive',
        'http://example.com/category/parent/2',
      ],
      [
        'http://example.com/category/id/1',
        'http://www.w3.org/1999/02/22-rdf-syntax-ns#type',
        'http://www.w3.org/2004/02/skos/core#Concept',
      ],
    ]);
    $this->drush('rdf_sync:synchronize', ['node']);
    $this->assertTriples('http://data', [
      [
        'http://example.com/page/id/1',
        'http://www.w3.org/1999/02/22-rdf-syntax-ns#type',
        'http://example.com/page',
      ],
      [
        'http://example.com/page/id/1',
        'http://example.com/page/title',
        '"Mad Men"@en',
      ],
      [
        'http://example.com/page/id/1',
        'http://example.com/page/category',
        'http://example.com/category/id/1',
      ],
      [
        'http://example.com/page/id/1',
        'http://example.com/page/body',
        '"Strong is stripped while <em>Emphasis</em> is kept"@en',
      ],
    ]);

    $this->toggleSynchronizationInDrush('enable');
    $node->delete();
    $term->delete();

    $this->assertNoTriples('http://data', [
      [
        'http://example.com/category/id/1',
        'http://www.w3.org/2004/02/skos/core#inScheme',
        'http://example.com/category',
      ],
      [
        'http://example.com/category/id/1',
        'http://www.w3.org/2004/02/skos/core#prefLabel',
        '"Series"@en',
      ],
      [
        'http://example.com/category/id/1',
        'http://www.w3.org/2004/02/skos/core#definition',
        '"7 seasons"@en',
      ],
      [
        'http://example.com/category/id/1',
        'http://www.w3.org/1999/02/22-rdf-syntax-ns#type',
        'http://www.w3.org/2004/02/skos/core#Concept',
      ],
      [
        'http://example.com/category/id/1',
        'http://www.w3.org/2004/02/skos/core#broaderTransitive',
        'http://example.com/category/parent/1',
      ],
      [
        'http://example.com/category/id/1',
        'http://www.w3.org/2004/02/skos/core#broaderTransitive',
        'http://example.com/category/parent/2',
      ],
      [
        'http://example.com/page/id/1',
        'http://www.w3.org/1999/02/22-rdf-syntax-ns#type',
        'http://example.com/page',
      ],
      [
        'http://example.com/page/id/1',
        'http://example.com/page/title',
        '"Mad Men"@en',
      ],
      [
        'http://example.com/page/id/1',
        'http://example.com/page/category',
        'http://example.com/category/id/1',
      ],
      [
        'http://example.com/page/id/1',
        'http://example.com/page/body',
        '"Strong is stripped while <em>Emphasis</em> is kept"@en',
      ],
    ]);
  }

  /**
   * Toggle synchronization in Drush.
   *
   * @param string $action
   *   Whether to enable or disable synchronization.
   */
  protected function toggleSynchronizationInDrush(string $action): void {
    $this->drush('rdf_sync:' . $action);
    // Mimic the refresh of the request as the state is updated in a separate
    // process and the state in the container is already loaded and cached.
    $this->container->get('state')->resetCache();
    $this->assertEquals($action === 'disable', \Drupal::state()->get('rdf_sync.synchronization_disabled'));
  }

}
