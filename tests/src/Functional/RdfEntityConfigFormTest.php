<?php

declare(strict_types=1);

namespace Drupal\Tests\rdf_sync\Functional;

use Drupal\Tests\BrowserTestBase;
use Drupal\node\Entity\NodeType;

/**
 * Test entity config form.
 *
 * @group rdf_sync
 */
class RdfEntityConfigFormTest extends BrowserTestBase {

  /**
   * {@inheritdoc}
   */
  protected static $modules = [
    'rdf_sync',
    'node',
    'field_ui',
  ];

  /**
   * {@inheritdoc}
   */
  protected $defaultTheme = 'stark';

  /**
   * Tests Rdf sync configuration for entity.
   *
   * @covers \Drupal\rdf_sync\Form\Alter\BundleEntityFormAlter
   *
   * @throws \Exception
   */
  public function testEntityForm(): void {
    NodeType::create(['type' => 'page', 'name' => 'Page'])->save();

    $this->drupalLogin($this->rootUser);
    $this->drupalGet('/admin/structure/types/manage/page');

    $assert = $this->assertSession();

    $assert->pageTextContains('RDF sync');

    // Update config.
    $data = [
      'rdf_sync[enabled]' => '1',
      'rdf_sync[type]' => 'https://data.org/value',
      'rdf_sync[uri_field_name]' => 'uri_field_name',
      'rdf_sync[uri_plugin]' => '',
      'rdf_sync[base_fields_mapping][type][target_id][predicate]' => 'https://data.org/type',
      'rdf_sync[base_fields_mapping][type][target_id][type]' => 'xsd:string',
      'rdf_sync[base_fields_mapping][status][value][predicate]' => 'https://data.org/status',
      'rdf_sync[base_fields_mapping][status][value][type]' => 'xsd:integer',
      'rdf_sync[base_fields_mapping][title][value][predicate]' => 'https://data.org/title',
      'rdf_sync[base_fields_mapping][title][value][type]' => 'xsd:string',
      'rdf_sync[base_fields_mapping][created][value][predicate]' => 'https://data.org/created',
      'rdf_sync[base_fields_mapping][created][value][type]' => 'xsd:dateTime',
    ];
    $this->submitForm($data, 'Save');

    // Check if data were saved.
    $this->drupalGet('/admin/structure/types/manage/page');
    $session = $this->assertSession();
    foreach ($data as $field => $value) {
      $session->fieldValueEquals($field, $value);
    }
  }

}
