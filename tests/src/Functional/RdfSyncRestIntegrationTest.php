<?php

declare(strict_types=1);

namespace Drupal\Tests\rdf_sync\Functional;

use Drupal\Core\Url;
use Drupal\Tests\BrowserTestBase;
use Drupal\Tests\rdf_sync\Traits\RdfSyncTestingDataTrait;
use Drupal\rdf_sync\Model\RdfSyncFormat;
use Drupal\rest\Entity\RestResourceConfig;

/**
 * Tests REST module integration.
 *
 * @group rdf_sync
 */
class RdfSyncRestIntegrationTest extends BrowserTestBase {

  use RdfSyncTestingDataTrait;

  /**
   * {@inheritdoc}
   */
  protected static $modules = [
    'node',
    'taxonomy',
    'serialization',
    'basic_auth',
    'rest',
    'rdf_sync',
    'rdf_sync_test',
  ];

  /**
   * {@inheritdoc}
   */
  protected $defaultTheme = 'stark';

  /**
   * {@inheritdoc}
   */
  protected function setUp(): void {
    parent::setUp();
    $this->setUpFields();

    // Enable cookie-based authentication and add all formats for the
    // entity:node REST resource.
    $resource_config = RestResourceConfig::load('entity.node');
    $configuration = $resource_config->get('configuration');
    $configuration['authentication'][] = 'cookie';
    $additional_formats = array_map(static fn($case) => $case->value, RdfSyncFormat::cases());
    $configuration['formats'] = array_merge($configuration['formats'], $additional_formats);
    $resource_config->set('configuration', $configuration)->save();
    $this->rebuildAll();

    $account = $this->drupalCreateUser(['access content']);
    $this->drupalLogin($account);
  }

  /**
   * Tests REST module routes.
   *
   * @dataProvider providerTestRestRouteSerialization
   */
  public function testRestRouteSerialization(string $format): void {
    // Test mapped node bundle route, assert 200 response.
    [, $node] = self::createEntities();
    $this->drupalGet($node->toUrl()->setOption('query', ['_format' => $format]));
    $this->assertSession()->statusCodeEquals(200);

    // Test unmapped node bundle route, assert 406 Not Acceptable response.
    $node = $this->drupalCreateNode(['type' => 'article']);
    $this->drupalGet($node->toUrl()->setOption('query', ['_format' => $format]));
    $this->assertSession()->statusCodeEquals(406);
    $this->assertSession()->responseHeaderEquals('Content-Type', 'text/plain; charset=UTF-8');

    // Test non-resource route, assert 404 response.
    /* @see \Drupal\serialization\EventSubscriber\UserRouteAlterSubscriber::onRoutingAlterAddFormats */
    $this->drupalGet(Url::fromRoute('user.login_status.http')->setOption('query', ['_format' => $format]));
    $this->assertSession()->statusCodeEquals(404);
    $this->assertSession()->responseHeaderEquals('Content-Type', 'text/plain; charset=UTF-8');
  }

  /**
   * Provides test cases for self::testRestRouteSerialization()
   *
   * @return array
   *   A list of testing cases.
   *
   * @see self::testRestRouteSerialization()
   */
  public static function providerTestRestRouteSerialization(): array {
    return array_map(static fn($case) => [$case->value], RdfSyncFormat::cases());
  }

}
