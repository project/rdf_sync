<?php

declare(strict_types=1);

namespace Drupal\Tests\rdf_sync\Functional;

use Drupal\Tests\BrowserTestBase;
use Drupal\node\Entity\NodeType;

/**
 * Test field config form.
 *
 * @group rdf_sync
 */
class RdfFieldConfigFormTest extends BrowserTestBase {

  /**
   * {@inheritdoc}
   */
  protected static $modules = [
    'rdf_sync',
    'node',
    'field_ui',
  ];

  /**
   * {@inheritdoc}
   */
  protected $defaultTheme = 'stark';

  /**
   * Tests Rdf sync configuration for field.
   *
   * @covers \Drupal\rdf_sync\Form\Alter\FieldConfigFormAlter
   *
   * @throws \Exception
   */
  public function testFieldForm(): void {
    $node_type = NodeType::create(['type' => 'page', 'name' => 'Page']);
    $node_type->save();
    node_add_body_field($node_type);

    $this->drupalLogin($this->createUser([
      'administer node fields',
    ]));
    $this->drupalGet('/admin/structure/types/manage/page/fields/node.page.body');

    $assert = $this->assertSession();
    $assert->pageTextContains('RDF sync mapping');

    // Update config.
    $data = [
      'rdf_sync[value][predicate]' => 'https://data.org/value',
      'rdf_sync[value][type]' => 'resource',
      'rdf_sync[summary][predicate]' => 'https://data.org/summary',
      'rdf_sync[summary][type]' => 'xsd:string',
      'rdf_sync[format][predicate]' => 'https://data.org/format',
      'rdf_sync[format][type]' => 'xsd:string',
    ];
    $this->submitForm($data, 'Save settings');

    // Check if data were saved.
    $this->drupalGet('/admin/structure/types/manage/page/fields/node.page.body');
    $session = $this->assertSession();
    foreach ($data as $field => $value) {
      $session->fieldValueEquals($field, $value);
    }
  }

}
