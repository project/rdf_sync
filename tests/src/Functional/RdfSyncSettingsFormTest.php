<?php

declare(strict_types=1);

namespace Drupal\Tests\rdf_sync\Functional;

use Drupal\Tests\BrowserTestBase;

/**
 * @coversDefaultClass \Drupal\rdf_sync\Form\SettingsForm
 *
 * @group rdf_sync
 *
 * @todo Move to a JS test to check Ajax functionality.
 */
class RdfSyncSettingsFormTest extends BrowserTestBase {

  /**
   * {@inheritdoc}
   */
  protected static $modules = ['rdf_sync'];

  /**
   * {@inheritdoc}
   */
  protected $defaultTheme = 'stark';

  /**
   * Checks access.
   *
   * @throws \Exception
   */
  public function testAccessForm(): void {
    $this->drupalGet('admin/config/system/rdf-sync');
    $this->assertSession()->statusCodeEquals(403);
  }

  /**
   * Checks config form.
   *
   * @throws \Exception
   */
  public function testUpdates(): void {
    $this->drupalLogin($this->DrupalCreateUser([
      'administer site configuration',
    ]));
    $this->drupalGet('admin/config/system/rdf-sync');
    $this->assertSession()->statusCodeEquals(200);

    // Update config.
    $data = [
      'graph_uri' => 'http://data2',
      'id' => 'virtuoso',
      'config[scheme]' => 'http',
      'config[host]' => 'localhost',
      'config[port]' => '8890',
      'config[paths][query]' => 'sparql',
      'config[paths][update]' => 'sparql',
      'config[paths][graph_store]' => 'sparql-graph-crud',
    ];
    $this->submitForm($data, 'Save configuration');
    $this->assertSession()
      ->pageTextContains('The configuration options have been saved.');

    // Check if update was performed.
    $this->drupalGet('admin/config/system/rdf-sync');
    $session = $this->assertSession();
    $session->fieldValueEquals('graph_uri', 'http://data2');
  }

}
