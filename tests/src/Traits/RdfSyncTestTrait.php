<?php

declare(strict_types=1);

namespace Drupal\Tests\rdf_sync\Traits;

use Drupal\rdf_sync\Model\VirtuosoEndpoint;
use EasyRdf\Graph;
use PHPUnit\Framework\Assert;

/**
 * Tests reusable code.
 */
trait RdfSyncTestTrait {

  /**
   * Asserts that an RDF graph contains a list of triples.
   *
   * @param string $graph
   *   The graph URI.
   * @param array[] $triples
   *   A list of triples. Each entry is an array of subject, predicate, object.
   */
  protected function assertTriples(string $graph, array $triples): void {
    $this->triplesAssertionHelper(TRUE, $graph, $triples);
  }

  /**
   * Asserts that an RDF graph contains none from a list of triples.
   *
   * @param string $graph
   *   The graph URI.
   * @param array[] $triples
   *   A list of triples. Each entry is an array of subject, predicate, object.
   */
  protected function assertNoTriples(string $graph, array $triples): void {
    $this->triplesAssertionHelper(FALSE, $graph, $triples);
  }

  /**
   * Provides a helper fro triples assertion methods.
   *
   * @param bool $checkExistence
   *   TRUE to check the existence, FALSE the lack of triples.
   * @param string $graphUri
   *   The graph URI.
   * @param array[] $triples
   *   A list of triples. Each entry is an array of subject, predicate, object.
   */
  protected function triplesAssertionHelper(bool $checkExistence, string $graphUri, array $triples): void {
    // Make sure that synchronization has been performed. On the real site this
    // is performed automatically, at the end of the request, but in tests, the
    // request lasts until the end of test, thus we need to trigger it manually.
    \Drupal::service('rdf_sync.synchronizer')->destruct();

    /** @var \Drupal\rdf_sync\RdfSyncConnectionInterface $connection */
    $connection = \Drupal::service('rdf_sync.connection');

    $failures = [];
    foreach ($triples as $triple) {
      // Allow simplification of expected values.
      if (filter_var($triple[2], FILTER_VALIDATE_URL)) {
        $triple[2] = ['value' => $triple[2], 'type' => 'uri'];
      }
      elseif (preg_match('/^"([^"].*)"@([a-z].*)$/', $triple[2], $matches)) {
        $triple[2] = ['value' => $matches[1], 'type' => 'literal', 'lang' => $matches[2]];
      }

      $graph = (new Graph());
      $graph->add(...$triple);
      $tripleSerialized = trim($graph->serialise('ntriples'), ".\n ");
      $tripleExists = $connection->query("ASK WHERE { GRAPH <$graphUri> { $tripleSerialized } }")->getBoolean();
      if ($checkExistence xor $tripleExists) {
        $failures[] = $triple;
      }
    }
    $verb = $checkExistence ? 'missing from' : 'present in';
    Assert::assertEmpty($failures, "Triples $verb the triplestore:\n- " . implode("\n- ", $failures));
  }

  /**
   * Provides connector test cases.
   *
   * @return array
   *   A list of tested connectors. Each element is a tuple:
   *   - 0: The connector plugin ID.
   *   - 1: The connector plugin configuration.
   */
  public static function providerConnectorTestCases(): array {
    return [
      'ARC2' => ['arc2', []],
      'Virtuoso' => [
        'virtuoso',
        [
          'endpoint' => VirtuosoEndpoint::Basic->value,
          'scheme' => 'http',
          'host' => 'virtuoso',
          'port' => 8890,
          'paths' => [
            'query' => 'sparql',
            'update' => 'sparql',
            'graph_store' => 'sparql-graph-crud',
          ],
          'credentials' => [
            'user' => NULL,
            'password' => NULL,
          ],
        ],
      ],
    ];
  }

  /**
   * {@inheritdoc}
   */
  protected function tearDown(): void {
    \Drupal::getContainer()->get('rdf_sync.connection')->clearGraph();
    parent::tearDown();
  }

}
