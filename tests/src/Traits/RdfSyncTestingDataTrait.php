<?php

declare(strict_types=1);

namespace Drupal\Tests\rdf_sync\Traits;

use Drupal\Tests\field\Traits\EntityReferenceFieldCreationTrait;
use Drupal\field\Entity\FieldConfig;
use Drupal\field\Entity\FieldStorageConfig;
use Drupal\node\Entity\Node;
use Drupal\taxonomy\Entity\Term;

/**
 * Entity structure and data creation.
 */
trait RdfSyncTestingDataTrait {

  use EntityReferenceFieldCreationTrait;

  /**
   * Sets up the tests.
   */
  protected function setUpFields(): void {
    if (!FieldStorageConfig::loadByName('node', 'body')) {
      // Add a body field.
      FieldStorageConfig::create([
        'entity_type' => 'node',
        'type' => 'text_with_summary',
        'field_name' => 'body',
        'cardinality' => 1,
      ])->save();
    }
    FieldConfig::create([
      'entity_type' => 'node',
      'bundle' => 'page',
      'field_name' => 'body',
      'label' => $this->randomString(),
    ])
      // Using the computed 'processed' column instead of 'value' to get the
      // text format applied.
      ->setThirdPartySetting('rdf_sync', 'processed', [
        'predicate' => 'http://example.com/page/body',
        'type' => NULL,
      ])->save();

    $this->createEntityReferenceField('node', 'page', 'category', $this->randomString(), 'taxonomy_term', 'default', ['target_bundles' => ['category']]);
    FieldConfig::loadByName('node', 'page', 'category')
      ->setThirdPartySetting('rdf_sync', 'target_id', [
        'predicate' => 'http://example.com/page/category',
        'type' => 'resource',
      ])->save();

    // Start on clean.
    \Drupal::service('entity_field.manager')->clearCachedFieldDefinitions();
    \Drupal::service('entity_type.bundle.info')->clearCachedBundles();
  }

  /**
   * Creates testing entities.
   *
   * @return \Drupal\Core\Entity\ContentEntityInterface[]
   *   A list of testing entities.
   */
  protected static function createEntities(): array {
    $term = Term::create([
      'vid' => 'category',
      'name' => 'Series',
      'description' => [
        'value' => '<b>7</b> seasons',
        'format' => 'html',
      ],
      'parent' => [
        Term::create([
          'vid' => 'category',
          'name' => 'Series',
          'uri' => 'http://example.com/category/parent/1',
        ]),
        Term::create([
          'vid' => 'category',
          'name' => 'Series',
          'uri' => 'http://example.com/category/parent/2',
        ]),
      ],
      'uri' => 'http://example.com/category/id/1',
    ]);
    $term->save();
    $node = Node::create([
      'type' => 'page',
      'title' => 'Mad Men',
      'uri' => 'http://example.com/page/id/1',
      'body' => [
        'value' => '<strong>Strong</strong> is stripped while <em>Emphasis</em> is kept',
        'format' => 'html',
      ],
      'category' => $term,
    ]);
    $node->save();
    return [$term, $node];
  }

}
