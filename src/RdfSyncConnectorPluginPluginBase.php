<?php

declare(strict_types=1);

namespace Drupal\rdf_sync;

use Drupal\Component\Utility\NestedArray;
use Drupal\Core\Plugin\PluginBase;

/**
 * Base class for RDF Sync connector plugins.
 */
abstract class RdfSyncConnectorPluginPluginBase extends PluginBase implements RdfSyncConnectorPluginInterface {

  /**
   * {@inheritdoc}
   */
  public function getConfiguration(): array {
    return $this->configuration;
  }

  /**
   * {@inheritdoc}
   */
  public function setConfiguration(array $configuration): void {
    $this->configuration = NestedArray::mergeDeep($this->defaultConfiguration(), $configuration);
  }

  /**
   * {@inheritdoc}
   */
  public function defaultConfiguration(): array {
    return [];
  }

}
