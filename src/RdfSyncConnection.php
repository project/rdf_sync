<?php

declare(strict_types=1);

namespace Drupal\rdf_sync;

use Drupal\Core\Config\ConfigFactoryInterface;
use Drupal\Core\Config\ImmutableConfig;
use EasyRdf\Graph;
use EasyRdf\GraphStore;
use EasyRdf\Serialiser\Ntriples;
use EasyRdf\Sparql\Result;

/**
 * Defines rdf_sync.connection service.
 */
class RdfSyncConnection implements RdfSyncConnectionInterface {

  /**
   * RDF sync settings static cache.
   */
  protected ImmutableConfig $settings;

  public function __construct(
    protected readonly ConfigFactoryInterface $configFactory,
    protected readonly RdfSyncConnectorPluginManager $connectorManager,
  ) {}

  /**
   * {@inheritdoc}
   */
  public function query(string $query): Result {
    return $this->getPluginInstance()->query($query);
  }

  /**
   * {@inheritdoc}
   */
  public function update(string $query): Result {
    return $this->getPluginInstance()->update($query);
  }

  /**
   * {@inheritdoc}
   */
  public function clearGraph(): void {
    $uri = $this->getSettings()->get('graph_uri');
    $this->getPluginInstance()->clearGraph($uri);
  }

  /**
   * {@inheritdoc}
   */
  public function createGraphStore(): GraphStore {
    return $this->getPluginInstance()->createGraphStore();
  }

  /**
   * {@inheritdoc}
   */
  public function updateTriples(array $triples): void {
    // @todo Replace this with a single query.
    $this->deleteTriples(array_keys($triples));
    $this->insertTriples($triples);
  }

  /**
   * {@inheritdoc}
   */
  public function deleteTriples(array $uris): void {
    $serializer = new Ntriples();
    $clauses = array_map(
      function (string $uri) use ($serializer): string {
        return '?subject = ' . $serializer->serialiseValue(['value' => $uri, 'type' => 'uri']);
      },
      $uris,
    );

    $graphUri = $this->getSettings()->get('graph_uri');
    $clauses = implode(" ||\n", $clauses);

    $query = <<<QUERY
        DELETE FROM <$graphUri>
            { ?subject ?predicate ?object }
        WHERE {
            ?subject ?predicate ?object .
            FILTER (
                $clauses
            )
        }
        QUERY;

    $this->update($query);
  }

  /**
   * {@inheritdoc}
   */
  public function insertTriples(array $triples): void {
    $graph = new Graph();
    $graph->parse($triples, 'php');
    $triples = $graph->serialise('ntriples');
    $graphUri = $this->getSettings()->get('graph_uri');
    $query = <<<QUERY
      INSERT INTO <$graphUri> {
        $triples
      }
      QUERY;
    $this->update($query);
  }

  /**
   * Returns the currently configured RDF Sync connector plugin instance.
   *
   * @return \Drupal\rdf_sync\RdfSyncConnectorPluginInterface
   *   The currently configured RDF Sync connector plugin instance.
   *
   * @throws \Drupal\Component\Plugin\Exception\PluginException
   *   When the configured plugin doesn't exist.
   */
  protected function getPluginInstance(): RdfSyncConnectorPluginInterface {
    $connectorConfig = $this->getSettings()->get('connector');
    $connector = $this->connectorManager->createInstance($connectorConfig['id'], $connectorConfig['config']);
    assert($connector instanceof RdfSyncConnectorPluginInterface);
    return $connector;
  }

  /**
   * Returns the RDF Sync settings.
   *
   * @return \Drupal\Core\Config\ImmutableConfig
   *   The RDF Sync settings.
   */
  protected function getSettings(): ImmutableConfig {
    if (!isset($this->settings)) {
      $this->settings = $this->configFactory->get('rdf_sync.settings');
    }
    return $this->settings;
  }

}
