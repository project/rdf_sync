<?php

declare(strict_types=1);

namespace Drupal\rdf_sync;

use Drupal\Component\Render\MarkupInterface;
use Drupal\Core\Cache\CacheBackendInterface;
use Drupal\Core\Extension\ModuleHandlerInterface;
use Drupal\Core\Plugin\DefaultPluginManager;
use Drupal\rdf_sync\Attribute\RdfSyncConnector;

/**
 * Plugin manager for RDf sync connector plugins.
 */
class RdfSyncConnectorPluginManager extends DefaultPluginManager {

  public function __construct(
    \Traversable $namespaces,
    CacheBackendInterface $cacheBackend,
    ModuleHandlerInterface $moduleHandler,
  ) {
    parent::__construct(
      'Plugin/rdf_sync/Connector',
      $namespaces,
      $moduleHandler,
      RdfSyncConnectorPluginInterface::class,
      RdfSyncConnector::class,
    );
    $this->alterInfo('rdf_sync_connector_info');
    $this->setCacheBackend($cacheBackend, 'rdf_sync_connector_plugins');
  }

  /**
   * {@inheritdoc}
   */
  public function createInstance($plugin_id, array $configuration = []): RdfSyncConnectorPluginInterface {
    // Extending parent just to get a nice return typing.
    $instance = parent::createInstance($plugin_id, $configuration);
    assert($instance instanceof RdfSyncConnectorPluginInterface);
    return $instance;
  }

  /**
   * Returns the list of definition labels as options.
   *
   * @return array
   *   An option list having the plugin ID as key and annotation type as value.
   */
  public function getDefinitionsAsOptions(): array {
    return array_map(
      fn (array $definition): MarkupInterface|string => $definition['label'],
      $this->getDefinitions(),
    );
  }

}
