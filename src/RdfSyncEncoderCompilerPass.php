<?php

declare(strict_types=1);

namespace Drupal\rdf_sync;

use Drupal\rdf_sync\Encoder\RdfSyncEncoder;
use Drupal\rdf_sync\Model\RdfSyncFormat;
use EasyRdf\Format;
use Symfony\Component\DependencyInjection\Compiler\CompilerPassInterface;
use Symfony\Component\DependencyInjection\ContainerBuilder;
use Symfony\Component\DependencyInjection\Definition;

/**
 * Creates an RDF encoder service for each format from rdf_sync container param.
 */
class RdfSyncEncoderCompilerPass implements CompilerPassInterface {

  /**
   * {@inheritdoc}
   */
  public function process(ContainerBuilder $container): void {
    $rdfSerializers = Format::getFormats();
    foreach (RdfSyncFormat::cases() as $format) {
      if (isset($rdfSerializers[$format->value])) {
        $definition = (new Definition(RdfSyncEncoder::class))->addTag('encoder', [
          'format' => $format->value,
        ]);
        $container->addDefinitions(["rdf_sync.encoder.$format->value" => $definition]);
      }
    }
  }

}
