<?php

declare(strict_types=1);

namespace Drupal\rdf_sync\Form;

use Drupal\Core\Entity\EntityTypeBundleInfoInterface;
use Drupal\Core\Entity\EntityTypeManagerInterface;
use Drupal\Core\Form\FormBase;
use Drupal\Core\Form\FormStateInterface;
use Drupal\rdf_sync\BatchSynchronizer;
use Drupal\rdf_sync\RdfSyncConnectionInterface;
use Drupal\rdf_sync\RdfSyncMapperInterface;
use Drupal\rdf_sync\RdfSyncSynchronizer;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * The synchronization form.
 */
class SynchronizationForm extends FormBase {

  public function __construct(
    protected RdfSyncSynchronizer $synchronizer,
    protected EntityTypeManagerInterface $entityTypeManager,
    protected EntityTypeBundleInfoInterface $entityTypeBundleInfo,
    protected BatchSynchronizer $batchSynchronizer,
    protected RdfSyncConnectionInterface $connection,
    protected RdfSyncMapperInterface $mapper,
  ) {}

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container): self {
    return new static(
      $container->get('rdf_sync.synchronizer'),
      $container->get('entity_type.manager'),
      $container->get('entity_type.bundle.info'),
      $container->get('rdf_sync.batch_synchronizer'),
      $container->get('rdf_sync.connection'),
      $container->get('rdf_sync.mapper'),
    );
  }

  /**
   * {@inheritdoc}
   */
  public function getFormId(): string {
    return 'rdf_synchronization_form';
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state): array {
    $isEnabled = $this->synchronizer->isSynchronizationEnabled();

    $form['sync_settings'] = [
      '#type' => 'fieldset',
      '#title' => $this->t('Settings'),
    ];
    $form['sync_settings']['enable_sync'] = [
      '#type' => 'checkbox',
      '#title' => $this->t('Enable synchronization'),
      '#default_value' => $isEnabled,
      '#description' => $this->t('Toggle synchronization on or off.'),
    ];
    $form['sync_settings']['save_settings'] = [
      '#type' => 'submit',
      '#value' => $this->t('Save settings'),
      '#submit' => ['::submitSaveSettings'],
      '#limit_validation_errors' => [['enable_sync']],
    ];

    $form['graph_management'] = [
      '#type' => 'fieldset',
      '#title' => $this->t('Graph management'),
    ];
    $form['graph_management']['empty_graph'] = [
      '#type' => 'submit',
      '#value' => $this->t('Empty RDF Graph'),
      '#submit' => ['::submitGraphManagement'],
      '#limit_validation_errors' => [],
    ];

    $form['sync_actions'] = [
      '#type' => 'fieldset',
      '#title' => $this->t('Synchronization'),
    ];
    $selectedEntityType = $form_state->getValue('entity_type');
    $form['sync_actions']['entity_type'] = [
      '#type' => 'select',
      '#title' => $this->t('Entity type'),
      '#options' => $this->getEntityTypes(),
      '#required' => TRUE,
      '#default_value' => $selectedEntityType,
      '#ajax' => [
        'callback' => '::updateBundles',
        'wrapper' => 'bundle-selector-wrapper',
      ],
    ];
    $form['sync_actions']['wrapper'] = [
      '#type' => 'container',
      '#attributes' => ['id' => 'bundle-selector-wrapper'],
    ];

    if ($selectedEntityType) {
      $form['sync_actions']['wrapper']['bundles'] = [
        '#type' => 'checkboxes',
        '#required' => TRUE,
        '#title' => $this->t('Bundles'),
        '#options' => $this->getBundles($selectedEntityType),
      ];
    }

    $form['sync_actions']['sync'] = [
      '#type' => 'submit',
      '#value' => $this->t('Perform synchronization'),
      '#submit' => ['::submitSyncActions'],
    ];

    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state): void {}

  /**
   * Form submission handler for save settings.
   *
   * @param array $form
   *   An associative array containing the structure of the form.
   * @param \Drupal\Core\Form\FormStateInterface $form_state
   *   The current state of the form.
   */
  public function submitSaveSettings(array &$form, FormStateInterface $form_state): void {
    $isEnabled = $form_state->getValue('enable_sync');

    if ($isEnabled) {
      $this->synchronizer->enableSynchronization();
    }
    else {
      $this->synchronizer->disableSynchronization();
    }
    $this->messenger()
      ->addMessage($this->t('Settings have been updated.'));
  }

  /**
   * Form submission handler for graph management.
   *
   * @param array $form
   *   An associative array containing the structure of the form.
   */
  public function submitGraphManagement(array &$form): void {
    $this->connection->clearGraph();
    $this->messenger()
      ->addMessage($this->t('The RDF graphs have been cleared.'));
  }

  /**
   * Form submission handler for sync actions.
   *
   * @param array $form
   *   An associative array containing the structure of the form.
   * @param \Drupal\Core\Form\FormStateInterface $form_state
   *   The current state of the form.
   */
  public function submitSyncActions(array &$form, FormStateInterface $form_state): void {
    $entityType = $form_state->getValue('entity_type');
    $bundles = array_filter($form_state->getValue('bundles', []));

    $this->batchSynchronizer->initBatch($entityType, $bundles);
    $this->messenger()->addMessage($this->t('Synchronization completed.'));
  }

  /**
   * Gets available entity types.
   *
   * @return array
   *   Entity type ids.
   */
  protected function getEntityTypes(): array {
    $entityTypes = $this->entityTypeManager->getDefinitions();
    $options = [];
    foreach ($entityTypes as $entityTypeId => $entityType) {
      $bundleOptions = $this->getBundles($entityTypeId);
      if (empty($bundleOptions)) {
        continue;
      }

      $options[$entityTypeId] = $entityType->getLabel();
    }
    return $options;
  }

  /**
   * Gets bundles for a specific entity type.
   *
   * @param string $entityTypeId
   *   Entity type id.
   */
  protected function getBundles(string $entityTypeId): array {
    $bundleInfo = $this->entityTypeBundleInfo
      ->getBundleInfo($entityTypeId);
    $options = [];

    foreach ($bundleInfo as $bundleId => $bundle) {
      if (!$this->mapper->isMappedBundle($entityTypeId, $bundleId)) {
        continue;
      }

      $options[$bundleId] = $bundle['label'];
    }

    return $options;
  }

  /**
   * AJAX callback to update bundles.
   *
   * @param array $form
   *   An associative array containing the structure of the form.
   */
  public function updateBundles(array $form): array {
    return $form['sync_actions']['wrapper'];
  }

}
