<?php

declare(strict_types=1);

namespace Drupal\rdf_sync\Form\Alter;

use Drupal\Core\Form\FormStateInterface;

/**
 * Form alter helper.
 */
interface FormAlterInterface {

  /**
   * Alters form.
   *
   * @param array $form
   *   Form render array.
   * @param \Drupal\Core\Form\FormStateInterface $formState
   *   Form state.
   */
  public function alter(array &$form, FormStateInterface $formState): void;

}
