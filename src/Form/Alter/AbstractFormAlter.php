<?php

declare(strict_types=1);

namespace Drupal\rdf_sync\Form\Alter;

use Drupal\Core\StringTranslation\StringTranslationTrait;
use Drupal\rdf_sync\Model\ColumnMapping;

/**
 * Abstract form alter.
 */
abstract class AbstractFormAlter implements FormAlterInterface {

  use StringTranslationTrait;

  /**
   * Gets predicate element.
   *
   * @param array $settings
   *   Field settings.
   *
   * @return array
   *   Rendered array.
   */
  protected function getPredicateElement(array $settings): array {
    return [
      '#type' => 'url',
      '#title' => $this->t('Predicate'),
      '#default_value' => $settings['predicate'] ?? NULL,
    ];
  }

  /**
   * Gets type element.
   *
   * @param array $settings
   *   Field settings.
   *
   * @return array
   *   Rendered array.
   */
  protected function getTypeElement(array $settings): array {
    return [
      '#type' => 'select',
      '#title' => $this->t('Type'),
      '#options' => array_combine(ColumnMapping::ALLOWED_TYPES, ColumnMapping::ALLOWED_TYPES),
      '#empty_value' => '',
      '#default_value' => $settings['type'] ?? NULL,
    ];
  }

}
