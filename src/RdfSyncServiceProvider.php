<?php

declare(strict_types=1);

namespace Drupal\rdf_sync;

use Drupal\Core\DependencyInjection\ContainerBuilder;
use Drupal\Core\DependencyInjection\ServiceProviderInterface;
use Drupal\Core\StackMiddleware\NegotiationMiddleware;
use Drupal\rdf_sync\Encoder\RdfSyncEncoder;
use Symfony\Component\DependencyInjection\Compiler\PassConfig;

/**
 * Adds a compiler pass to dependency injection container.
 */
class RdfSyncServiceProvider implements ServiceProviderInterface {

  /**
   * {@inheritdoc}
   */
  public function register(ContainerBuilder $container): void {
    // Run this compiler acts before RegisterSerializationClassesCompilerPass so
    // that services created by this pass, and tagged as 'encoder', are picked
    // up by RegisterSerializationClassesCompilerPass.
    // @see \Drupal\serialization\RegisterSerializationClassesCompilerPass
    $container->addCompilerPass(new RdfSyncEncoderCompilerPass(), PassConfig::TYPE_BEFORE_OPTIMIZATION, 10);
    $container->addCompilerPass(new RdfSyncEncoderFormatsCompilerPass(), PassConfig::TYPE_REMOVE);

    // Register new content types.
    if ($container->has('http_middleware.negotiation') && is_a($container->getDefinition('http_middleware.negotiation')->getClass(), NegotiationMiddleware::class, TRUE)) {
      $definition = $container->getDefinition('http_middleware.negotiation');
      foreach (RdfSyncEncoder::getSupportedFormats() as $format) {
        $definition->addMethodCall('registerFormat', [$format->getName(), array_keys($format->getMimeTypes())]);
      }
    }
  }

}
