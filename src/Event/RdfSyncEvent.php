<?php

declare(strict_types=1);

namespace Drupal\rdf_sync\Event;

use Drupal\rdf_sync\Model\SyncMethod;
use Symfony\Contracts\EventDispatcher\Event;

/**
 * Occurs before syncing the entities to the endpoint.
 *
 * Subscribers are able to alter the list of entities to be synced or perform
 * additional actions.
 */
class RdfSyncEvent extends Event {

  public function __construct(
    public readonly SyncMethod $syncMethod,
    protected array $entities,
  ) {}

  /**
   * Returns the objects to be synced.
   *
   * @return \Drupal\Core\Entity\ContentEntityInterface[]
   *   The objects to be synced.
   */
  public function getEntities(): array {
    return $this->entities;
  }

  /**
   * Sets the objects to be synced.
   *
   * @param \Drupal\Core\Entity\ContentEntityInterface[] $entities
   *   The objects to be synced.
   *
   * @return $this
   */
  public function setEntities(array $entities): self {
    $this->entities = $entities;
    return $this;
  }

}
