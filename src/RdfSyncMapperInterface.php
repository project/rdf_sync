<?php

declare(strict_types=1);

namespace Drupal\rdf_sync;

use Drupal\Core\Entity\ContentEntityInterface;
use Drupal\Core\Entity\EntityInterface;

/**
 * Interface for 'rdf_sync.mapper' service.
 */
interface RdfSyncMapperInterface {

  /**
   * Checks whether a given entity type has at least 1 bundle mapped.
   *
   * @param string $entityTypeId
   *   The entity type ID.
   *
   * @return bool
   *   True if the entity type has at least 1 bundle mapped, false otherwise.
   */
  public function entityTypeHasMappings(string $entityTypeId): bool;

  /**
   * Returns the RDF type of given bundle.
   *
   * The caller should pass either $entityTypeId and $bundle or the $entity.
   *
   * @param string|null $entityTypeId
   *   The bundle's entity type ID.
   * @param string|null $bundle
   *   The bundle.
   * @param \Drupal\Core\Entity\ContentEntityInterface|null $entity
   *   The entity.
   */
  public function getBundleRdfType(?string $entityTypeId = NULL, ?string $bundle = NULL, ?ContentEntityInterface $entity = NULL): ?string;

  /**
   * Checks whether a given bundle is mapped.
   *
   * @param string $entityTypeId
   *   The bundle's entity type ID.
   * @param string $bundle
   *   The bundle.
   *
   * @return bool
   *   Whether a given bundle is mapped.
   */
  public function isMappedBundle(string $entityTypeId, string $bundle): bool;

  /**
   * Checks whether an entity is mapped.
   *
   * @param \Drupal\Core\Entity\EntityInterface $entity
   *   The entity.
   *
   * @return bool
   *   Whether the entity is mapped.
   */
  public function isMappedEntity(EntityInterface $entity): bool;

  /**
   * Returns the mappings of given bundle.
   *
   * The caller should pass either $entityTypeId and $bundle or the $entity.
   *
   * @param string|null $entityTypeId
   *   The bundle's entity type ID.
   * @param string|null $bundle
   *   The bundle.
   * @param \Drupal\Core\Entity\ContentEntityInterface|null $entity
   *   The entity.
   *
   * @return \Drupal\rdf_sync\Model\ColumnMapping[][]
   *   Mappings list.
   */
  public function getMappings(?string $entityTypeId = NULL, ?string $bundle = NULL, ?ContentEntityInterface $entity = NULL): array;

  /**
   * Returns the URI field name.
   *
   * The caller should pass either $entityTypeId and $bundle or the $entity.
   *
   * @param string|null $entityTypeId
   *   The bundle's entity type ID.
   * @param string|null $bundle
   *   The bundle.
   * @param \Drupal\Core\Entity\ContentEntityInterface|null $entity
   *   The entity.
   *
   * @return string
   *   The URI field name.
   */
  public function getRdfUriFieldName(?string $entityTypeId = NULL, ?string $bundle = NULL, ?ContentEntityInterface $entity = NULL): string;

  /**
   * Returns the bundle RDF URI plugin ID.
   *
   * @param string $entityTypeId
   *   The bundle's entity type ID.
   * @param string $bundle
   *   The bundle.
   *
   * @return array|null
   *   The bundle RDF URI plugin ID.
   */
  public function getRdfUriPluginId(string $entityTypeId, string $bundle): ?string;

  /**
   * Checks whether an entity did map the bundle.
   *
   * @param \Drupal\Core\Entity\ContentEntityInterface $entity
   *   The entity.
   *
   * @return bool
   *   Whether an entity did map the bundle.
   */
  public function hasBundleMapping(ContentEntityInterface $entity): bool;

  /**
   * Returns an entity given its URI.
   *
   * @param string $uri
   *   The entity URI.
   *
   * @return \Drupal\Core\Entity\ContentEntityInterface|null
   *   The entity or NULL if it cannot be retrieved.
   */
  public function getEntityByUri(string $uri): ?ContentEntityInterface;

  /**
   * Returns a list entities given a list of URIs.
   *
   * @param string[] $uris
   *   List of URIs.
   *
   * @return \Drupal\Core\Entity\ContentEntityInterface[]
   *   Associative array of entities keyed by their URI.
   */
  public function getEntitiesByUris(array $uris): array;

  /**
   * Returns a list of URIs keyed by their entity ID.
   *
   * @param string $entityTypeId
   *   The entity type ID.
   * @param string|null $bundle
   *   (optional) If provided, the result will be narrowed to this bundle.
   *
   * @return array
   *   A list of URIs keyed by the entity ID.
   */
  public function getUrisByEntityType(string $entityTypeId, ?string $bundle = NULL): array;

}
