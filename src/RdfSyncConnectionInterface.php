<?php

declare(strict_types=1);

namespace Drupal\rdf_sync;

use EasyRdf\GraphStore;
use EasyRdf\Sparql\Result;

/**
 * Provides an interface for the rdf_sync.connection service.
 */
interface RdfSyncConnectionInterface {

  /**
   * Runs a SPARQL query and returns the results.
   *
   * @param string $query
   *   The SPARQL query.
   *
   * @return \EasyRdf\Sparql\Result
   *   The result object.
   */
  public function query(string $query): Result;

  /**
   * Runs a SPARQL update and returns the results.
   *
   * @param string $query
   *   The SPARQL query.
   *
   * @return \EasyRdf\Sparql\Result
   *   The result object.
   */
  public function update(string $query): Result;

  /**
   * Clear the content of an entire graph.
   */
  public function clearGraph(): void;

  /**
   * Creates a graph store.
   *
   * @return \EasyRdf\GraphStore
   *   Graph store.
   */
  public function createGraphStore(): GraphStore;

  /**
   * Deletes a list of triples from the SPARQL backend given their subjects.
   *
   * @param array $uris
   *   A list of triple URIs to be deleted.
   */
  public function deleteTriples(array $uris): void;

  /**
   * Inserts a list of triples in the SPARQL triple-store.
   *
   * @param array $triples
   *   An array of triples structured as the return of Graph::toRdfPhp().
   *
   * @see \EasyRdf\Graph::toRdfPhp
   */
  public function insertTriples(array $triples): void;

  /**
   * Updates a list of triples in the SPARQL triple-store.
   *
   * @param array $triples
   *   An array of triples structured as the return of Graph::toRdfPhp().
   *
   * @see \EasyRdf\Graph::toRdfPhp
   */
  public function updateTriples(array $triples): void;

}
