<?php

declare(strict_types=1);

namespace Drupal\rdf_sync\Model;

/**
 * Synchronization method.
 *
 * @phpcs:disable Drupal.NamingConventions.ValidEnumCase.NoUpperAcronyms
 */
enum SyncMethod {

  case INSERT;
  case UPDATE;
  case DELETE;

}
