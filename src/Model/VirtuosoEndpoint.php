<?php

declare(strict_types=1);

namespace Drupal\rdf_sync\Model;

/**
 * Virtuoso endpoint type.
 *
 * @see https://vos.openlinksw.com/owiki/wiki/VOS/VirtTipsAndTricksGuideSPARQLEndpoints
 */
enum VirtuosoEndpoint: string {

  case Basic = 'basic';
  case Digest = 'digest';
  case OAuth = 'oauth';

  /**
   * Returns a human-readable enum array.
   *
   * @return array
   *   Array keyed by enum value, having a human-readable as value.
   */
  public static function casesAsOptions(): array {
    return [
      self::Basic->value => t('Basic default'),
      self::Digest->value => t('Digest authentication'),
      self::OAuth->value => t('OAuth authentication'),
    ];
  }

}
