<?php

declare(strict_types=1);

namespace Drupal\rdf_sync\Model;

/**
 * Supported formats.
 *
 * @phpcs:disable Drupal.NamingConventions.ValidEnumCase.NoUpperAcronyms
 */
enum RdfSyncFormat: string {

  case JSONLD = 'jsonld';
  case N3 = 'n3';
  case NTRIPLES = 'ntriples';
  case RDFXML = 'rdfxml';
  case TURTLE = 'turtle';

}
