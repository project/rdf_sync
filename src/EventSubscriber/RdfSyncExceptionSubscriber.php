<?php

declare(strict_types=1);

namespace Drupal\rdf_sync\EventSubscriber;

use Drupal\Core\EventSubscriber\MainContentViewSubscriber;
use Drupal\rdf_sync\Model\RdfSyncFormat;
use Drupal\serialization\EventSubscriber\DefaultExceptionSubscriber;
use Symfony\Component\HttpKernel\Event\ExceptionEvent;
use Symfony\Component\HttpKernel\Exception\HttpException;
use Symfony\Component\HttpKernel\Exception\NotAcceptableHttpException;
use Symfony\Component\Serializer\Exception\NotEncodableValueException;

/**
 * Handles serialization exceptions for rdf_sync formats.
 */
class RdfSyncExceptionSubscriber extends DefaultExceptionSubscriber {

  /**
   * {@inheritdoc}
   */
  protected static function getPriority(): int {
    // Execute before ExceptionLoggingSubscriber::onException (priority: 50).
    /* @see \Drupal\Core\EventSubscriber\ExceptionLoggingSubscriber::onException */
    return 70;
  }

  /**
   * {@inheritdoc}
   */
  protected function getHandledFormats(): array {
    return array_map(static fn($case) => $case->value, RdfSyncFormat::cases());
  }

  /**
   * {@inheritdoc}
   */
  public function onException(ExceptionEvent $event): void {
    if (!$this->applies($event)) {
      return;
    }

    $exception = $event->getThrowable();
    if (!$exception instanceof HttpException) {
      $exception = $exception instanceof NotEncodableValueException
        ? new NotAcceptableHttpException($exception->getMessage(), $exception)
        : new HttpException(500, $exception->getMessage(), $exception);
      $event->setThrowable($exception);
    }
  }

  /**
   * Check if the error applies to rdf_sync.
   */
  protected function applies(ExceptionEvent $event): bool {
    $request = $event->getRequest();
    $format = $request->query->get(MainContentViewSubscriber::WRAPPER_FORMAT, $request->getRequestFormat());
    return \in_array($format, $this->getHandledFormats(), TRUE);
  }

}
