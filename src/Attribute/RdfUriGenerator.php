<?php

declare(strict_types=1);

namespace Drupal\rdf_sync\Attribute;

use Drupal\Component\Plugin\Attribute\Plugin;
use Drupal\Core\StringTranslation\TranslatableMarkup;

/**
 * Defines an RdfUriGenerator.
 */
#[\Attribute(\Attribute::TARGET_CLASS)]
class RdfUriGenerator extends Plugin {

  /**
   * Constructs an RDF URI generator attribute.
   *
   * @param string $id
   *   The plugin ID.
   * @param \Drupal\Core\StringTranslation\TranslatableMarkup $label
   *   The human-readable name of the widget type.
   */
  public function __construct(
    public readonly string $id,
    public readonly TranslatableMarkup $label,
  ) {}

}
