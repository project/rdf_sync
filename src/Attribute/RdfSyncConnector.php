<?php

declare(strict_types=1);

namespace Drupal\rdf_sync\Attribute;

use Drupal\Component\Plugin\Attribute\Plugin;
use Drupal\Core\StringTranslation\TranslatableMarkup;

/**
 * Defines an RDF Sync connector attribute for plugin discovery.
 *
 * Plugin Namespace: Plugin\rdf_sync\Connector.
 *
 * @ingroup field_widget
 */
#[\Attribute(\Attribute::TARGET_CLASS)]
class RdfSyncConnector extends Plugin {

  /**
   * Constructs an RDF Sync connector attribute.
   *
   * @param string $id
   *   The plugin ID.
   * @param \Drupal\Core\StringTranslation\TranslatableMarkup $label
   *   The human-readable name of the widget type.
   * @param \Drupal\Core\StringTranslation\TranslatableMarkup|null $description
   *   (optional) A short description of the widget type.
   * @param string|null $website
   *   (optional) The connector website.
   */
  public function __construct(
    public readonly string $id,
    public readonly TranslatableMarkup $label,
    public readonly ?TranslatableMarkup $description = NULL,
    public readonly ?string $website = NULL,
  ) {}

}
