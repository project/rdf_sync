<?php

declare(strict_types=1);

namespace Drupal\rdf_sync;

use Drupal\rdf_sync\Model\RdfSyncFormat;
use Symfony\Component\DependencyInjection\Compiler\CompilerPassInterface;
use Symfony\Component\DependencyInjection\ContainerBuilder;

/**
 * Removes rdf_sync formats from 'serializer.formats' container parameter.
 *
 * - The REST module should only allow rdf_sync formats to be used for entity
 *   resources/routes. e.g.: We don't want to apply this to login/logout routes.
 * - The 'serialization.exception.default' service doesn't support our formats,
 *   hence a custom exception subscriber is needed. This also allows us to
 *   return 406 Not Acceptable when attempting to normalize an unmapped entity.
 *   The NotEncodableValueException results in a NotAcceptableHttpException.
 *
 * @see \Drupal\serialization\RegisterSerializationClassesCompilerPass
 * @see \Drupal\serialization\EventSubscriber\UserRouteAlterSubscriber::onRoutingAlterAddFormats
 * @see \Drupal\serialization\EventSubscriber\DefaultExceptionSubscriber::on4xx
 * @see \Drupal\rdf_sync\EventSubscriber\RdfSyncExceptionSubscriber::onException
 * @see https://www.drupal.org/project/drupal/issues/2979753
 */
class RdfSyncEncoderFormatsCompilerPass implements CompilerPassInterface {

  /**
   * {@inheritdoc}
   */
  public function process(ContainerBuilder $container): void {
    if ($container->hasParameter('serializer.formats')) {
      $rdf_sync_formats = array_map(static fn(RdfSyncFormat $format): string => $format->value, RdfSyncFormat::cases());
      $filtered_formats = array_diff($container->getParameter('serializer.formats'), $rdf_sync_formats);
      $container->setParameter('serializer.formats', array_values($filtered_formats));
    }
  }

}
