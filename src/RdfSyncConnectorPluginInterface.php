<?php

declare(strict_types=1);

namespace Drupal\rdf_sync;

use Drupal\Component\Plugin\ConfigurableInterface;
use Drupal\Component\Plugin\PluginInspectionInterface;
use EasyRdf\GraphStore;
use EasyRdf\Sparql\Result;

/**
 * Interface of RDF Sync connector plugins.
 */
interface RdfSyncConnectorPluginInterface extends PluginInspectionInterface, ConfigurableInterface {

  /**
   * Runs a SPARQL query and returns the results.
   *
   * @param string $query
   *   The SPARQL query.
   *
   * @return \EasyRdf\Sparql\Result
   *   The result object.
   */
  public function query(string $query): Result;

  /**
   * Runs a SPARQL update and returns the results.
   *
   * @param string $query
   *   The SPARQL query.
   *
   * @return \EasyRdf\Sparql\Result
   *   The result object.
   */
  public function update(string $query): Result;

  /**
   * Clear the content of an entire graph.
   *
   * @param string $uri
   *   The graph URI.
   */
  public function clearGraph(string $uri): void;

  /**
   * Creates a graph store.
   *
   * @return \EasyRdf\GraphStore
   *   Graph store.
   */
  public function createGraphStore(): GraphStore;

}
