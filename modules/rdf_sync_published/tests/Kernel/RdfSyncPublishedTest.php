<?php

declare(strict_types=1);

namespace Drupal\Tests\rdf_sync_published\Kernel;

use Drupal\KernelTests\KernelTestBase;
use Drupal\Tests\rdf_sync\Traits\RdfSyncTestTrait;
use Drupal\Tests\rdf_sync\Traits\RdfSyncTestingDataTrait;
use Drupal\rdf_sync\Model\SyncMethod;

/**
 * Tests the RDF sync by publication status.
 *
 * @group rdf_sync
 */
class RdfSyncPublishedTest extends KernelTestBase {

  use RdfSyncTestingDataTrait;
  use RdfSyncTestTrait;

  /**
   * {@inheritdoc}
   */
  protected static $modules = [
    'field',
    'filter',
    'node',
    'rdf_sync',
    'rdf_sync_published',
    'rdf_sync_test',
    'serialization',
    'system',
    'taxonomy',
    'text',
    'user',
  ];

  /**
   * {@inheritdoc}
   */
  protected function setUp(): void {
    parent::setUp();

    $this->installEntitySchema('taxonomy_term');
    $this->installEntitySchema('node');
    $this->installEntitySchema('user');
    $this->installSchema('node', ['node_access']);
    $this->installConfig(['rdf_sync_test', 'rdf_sync']);
    $this->installSchema('rdf_sync', ['rdf_sync_uri']);

    $this->setUpFields();
  }

  /**
   * @covers \Drupal\rdf_sync_published\EventSubscriber\RdfSyncPublishedSubscriber
   * @dataProvider providerConnectorTestCases
   *
   * @param string $connectorId
   *   The connector plugin ID.
   * @param array $connectorConfig
   *   The connector plugin configuration.
   */
  public function testSynchronization(string $connectorId, array $connectorConfig): void {
    $this->config('rdf_sync.settings')
      ->set('connector.id', $connectorId)
      ->set('connector.config', $connectorConfig)
      ->save();

    $node = $this->container->get('entity_type.manager')->getStorage('node')->create([
      'type' => 'page',
      'title' => 'Some title',
      // Initially create the node as unpublished.
      'status' => 0,
      'uri' => 'http://example.com/page/id/1',
    ]);
    $node->save();

    /** @var \Drupal\rdf_sync\RdfSyncSynchronizer $synchronizer */
    $synchronizer = $this->container->get('rdf_sync.synchronizer');
    $synchronizer->synchronize(SyncMethod::INSERT, [$node], TRUE);
    $this->assertNoTriples('http://data', [
      ['http://example.com/page/id/1', 'http://www.w3.org/1999/02/22-rdf-syntax-ns#type', 'http://example.com/page'],
      ['http://example.com/page/id/1', 'http://example.com/page/title', '"Some title"@en'],
    ]);

    // Publish the entity and synchronize again.
    $node->setPublished()->save();
    $synchronizer->synchronize(SyncMethod::INSERT, [$node], TRUE);
    $this->assertTriples('http://data', [
      ['http://example.com/page/id/1', 'http://www.w3.org/1999/02/22-rdf-syntax-ns#type', 'http://example.com/page'],
      ['http://example.com/page/id/1', 'http://example.com/page/title', '"Some title"@en'],
    ]);
  }

}
