<?php

declare(strict_types=1);

namespace Drupal\rdf_sync_published\EventSubscriber;

use Drupal\Core\Entity\EntityPublishedInterface;
use Drupal\rdf_sync\Event\RdfSyncEvent;
use Symfony\Component\EventDispatcher\EventSubscriberInterface;

/**
 * Listens to the event before syncing the entities.
 */
class RdfSyncPublishedSubscriber implements EventSubscriberInterface {

  /**
   * {@inheritdoc}
   */
  public static function getSubscribedEvents(): array {
    return [
      RdfSyncEvent::class => 'filterOutUnpublished',
    ];
  }

  /**
   * Filters out unpublished entities.
   *
   * @param \Drupal\rdf_sync\Event\RdfSyncEvent $event
   *   The event object.
   */
  public function filterOutUnpublished(RdfSyncEvent $event): void {
    $entities = $event->getEntities();
    // Do not filter out entities that do not implement
    // EntityPublishedInterface as we cannot determine their publication
    // status and might filter out entities that should be synced.
    $entities = array_filter($entities, fn ($entity): bool => !$entity instanceof EntityPublishedInterface || $entity->isPublished());
    $event->setEntities($entities);
  }

}
